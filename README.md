# Instant mail server for receiving e-mails

Aim of this project is to quickly setup a recipient mail server on a VPS, including MX record.

## Quickstart

1. Install Ansible dependencies, roles from [my Gitlab](https://gitlab.com/stemid-ansible/roles) and modules from Ansible's community collections:  ``ansible-galaxy install -r requirements.yml; ansible-galaxy collection install -r requirements.yml``
2. Install the python dependencies, for linode+route53 it's ``pip install --user boto linode_api4``
3. Setup the necessary access tokens, Linode, AWS and so forth
4. Run linode.yml to create the VPS (Because of [Github issue #1764](https://github.com/ansible-collections/community.general/issues/1764) you must ``export LINODE_ACCESS_TOKEN=your.token`` first)
5. Run route53.yml to create the DNS
6. Add the linode instance to your ssh config, linode.yml will print its IP
7. Run bootstrap.yml to prep the VPS with useful tools
8. Run site.yml to install and configure Postfix MTA
9. Set ``state: absent`` in ``inventory/default/group_vars/all.yml`` for your linode instance and re-run linode.yml and route53.yml to delete all instances and their DNS records.

## Settings

Copy the default environment to avoid conflicts when you try to update this git repo. Name your environment whatever you want, I'm naming mine Linode because I might have another for other hosts.

    cp -r inventory/default inventory/linode

Read the ``group_vars/all.yml`` file for settings but here is the gist.

* ``postfix_domain`` should match ``linode_instances[].zone`` because it's used in Postfix config as your recipient domain, and in Route53 as your DNS zone.
* ``linode_group`` must match groups in ``inventory/default/linode.yml`` inventory file.
* ``linode_instances`` is a list but technically these playbooks only support one server.
* ``linode_instances[].label`` is also used to setup an A record to your Linode instance and then point your MX record to it.

## Create instance and its DNS

    export LINODE_ACCESS_TOKEN=deadbeef
    ansible-playbook -i inventory/linode/linode.yml linode.yml
    ansible-playbook -i inventory/linode/linode.yml route53.yml

## Configure OS to receive e-mail

    ansible-playbook -i inventory/linode/linode.yml bootstrap.yml
    ansible-playbook -i inventory/linode/linode.yml site.yml

